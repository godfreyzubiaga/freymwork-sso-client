import { Spin } from 'antd';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import { LoadingOutlined } from '@ant-design/icons';

import { retrieveApplicationById } from './redux/Application/Application.actions';
import { SUCCESS_RETRIEVE_APPLICATION_BY_ID } from './redux/Application/Application.types';
import { retrieveConfig } from './redux/Config/Config.actions';
import { useTypedSelector } from './redux/root.reducer';
import { extractDataFromQuery, extractDataFromToken } from './utils';
import MyRoutes from './pages/Routes';
import { saveTokenToStore } from './redux/Authorization/Authorization.actions';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 100vh;
  width: 100vw;
`;

const App = () => {
  const configState = useTypedSelector((state) => state.configState);
  const applicationState = useTypedSelector((state) => state.applicationState);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (configState.config) {
      const token = extractDataFromQuery('token');
      const appId = extractDataFromQuery('appId');

      if (token) {
        const tokenData = extractDataFromToken(token);
        dispatch(saveTokenToStore(token));
        dispatch({ type: SUCCESS_RETRIEVE_APPLICATION_BY_ID, payload: tokenData });
      } else if (appId) {
        dispatch(retrieveApplicationById(appId));
      } else {
        dispatch(retrieveApplicationById(configState.config.defaultAppId));
      }
    }
  }, [configState.config]);

  React.useEffect(() => {
    if (!configState.config) {
      dispatch(retrieveConfig());
    }
  }, []);

  React.useEffect(() => {
    const { application, status } = applicationState;
    if (application && status.success && !status.loading) {
      const favicon = document.getElementById('favicon') as any;
      favicon.href = application.favIcon;
      if (application.appName !== 'Freymwork') {
        document.title = `${application.appName} - Freymwork-SSO`;
      }
    }
  }, [applicationState.application]);

  return (
    <Spin
      indicator={antIcon}
      size="large"
      tip="Retrieving Config..."
      spinning={configState.status.loading}
    >
      <Container>{!configState.status.loading && <MyRoutes />}</Container>
    </Spin>
  );
};

export default App;
