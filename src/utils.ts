import { notification } from 'antd';
import jwtDecode from 'jwt-decode';
import queryString from 'query-string';
import { useMediaQuery } from 'react-responsive';
import type { ApplicationData } from './redux/Application/Application.types';

export const extractDataFromQuery = (dataName: any): string => {
  const urlQueryData = queryString.parse(window.location.search) as any;
  return urlQueryData[dataName];
};

export const extractDataFromToken = (token: string): ApplicationData | undefined => {
  if (token) {
    const data = jwtDecode(token) as ApplicationData;
    const safeData = { ...data };
    return safeData;
  }
  return undefined;
};

export const isDesktopOrLaptop = () => useMediaQuery({
  query: '(min-width: 1130px)',
});

export const appendTokenQuery = (queryObject: object) => {
  const queryParams = queryString.stringify(queryObject);
  return queryObject ? `?${queryParams}` : '';
};

export const showAPIError = (message: string) => {
  notification.error({
    message,
    placement: 'bottomLeft',
  });
};
