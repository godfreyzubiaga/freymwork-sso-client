import {
  Button, Card, Divider, Form, Input, Spin, Typography,
} from 'antd';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { LoadingOutlined } from '@ant-design/icons';

import { loginAction } from '../../redux/Authorization/Authorization.actions';
import { LoginCredential } from '../../redux/Authorization/Authorization.types';
import { useTypedSelector } from '../../redux/root.reducer';
import { appendTokenQuery, isDesktopOrLaptop } from '../../utils';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const { Title, Text } = Typography;

const StyledDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 1350px) {
    text-align: center;
  }
`;

const LoginPage = () => {
  const { status: appStatus, application } = useTypedSelector((state) => state.applicationState);
  const { appId, status: authStatus, token } = useTypedSelector((state) => state.authState);
  const { status: configStatus } = useTypedSelector((state) => state.configState);
  const dispatch = useDispatch();

  const isLoading = () => appStatus.loading || authStatus.loading || configStatus.loading;

  const onFinish = (values: LoginCredential) => {
    dispatch(loginAction(values));
  };

  const getQueryParams = () => {
    if (token) {
      return { token };
    }
    if (appId) {
      return { appId };
    }
    return {};
  };

  return (
    <StyledDiv>
      <Spin indicator={antIcon} spinning={isLoading()}>
        <Card
          bordered={false}
          style={{ height: 500, width: isDesktopOrLaptop() ? 400 : 'auto', textAlign: 'center' }}
        >
          {/* {isLoading() ? (
            <Spin indicator={antIcon} />
          ) : ( */}
          <Title level={4}>{`${application?.appName} Login`}</Title>
          {/* )} */}

          <Divider plain>
            <Text type="secondary">Freymwork SSO</Text>
          </Divider>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            layout="vertical"
            onFinish={onFinish}
            autoComplete="off"
            style={{ textAlign: isDesktopOrLaptop() ? 'right' : 'center' }}
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item wrapperCol={isDesktopOrLaptop() ? { span: 24 } : {}}>
              <Button
                style={{ background: application?.colorTheme, border: 'none' }}
                type="primary"
                htmlType="submit"
                loading={authStatus.loading}
              >
                Login
              </Button>
              {application?.allowRegistration && (
                <div>
                  <Text>
                    Don&#39;t have an account?
                    <Link to={`/register${appendTokenQuery(getQueryParams())}`}>
                      <Button style={{ color: application?.colorTheme, padding: 0 }} type="link">
                        &nbsp;Register here
                      </Button>
                    </Link>
                  </Text>
                </div>
              )}
            </Form.Item>
          </Form>
        </Card>
      </Spin>
    </StyledDiv>
  );
};

export default LoginPage;
