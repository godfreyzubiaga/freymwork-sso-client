import { Button, Result } from 'antd';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

interface Props {
  width?: string;
  height?: string;
}

const ErrorContainer = styled.div<Props>`
  display: grid;
  place-self: center;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
`;

const Unavailable = ({ width, height }: Props) => {
  const navigate = useNavigate();
  return (
    <ErrorContainer width={width} height={height}>
      <Result
        status="500"
        title="500"
        subTitle="Sorry, something went wrong."
        extra={
          <Button onClick={() => navigate('/')} type="primary">
            ← Back Home
          </Button>
        }
      />
    </ErrorContainer>
  );
};

export default Unavailable;
