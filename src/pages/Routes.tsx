import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import React from 'react';
import { Route, Routes, BrowserRouter } from 'react-router-dom';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const LandingPage = React.lazy(() => import('./LandingPage/LandingPage'));
const PageNotFound = React.lazy(() => import('./PageNotFound/PageNotFound'));
const SSOAuthPages = React.lazy(() => import('./SSOAuthPages/SSOAuthPages'));

const MyRoutes = () => (
  <BrowserRouter>
    <React.Suspense fallback={<Spin indicator={antIcon} />}>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="*" element={<PageNotFound />} />
        <Route path="register" element={<SSOAuthPages component="register" />} />
        <Route path="login" element={<SSOAuthPages component="login" />} />
      </Routes>
    </React.Suspense>
  </BrowserRouter>
);

export default MyRoutes;
