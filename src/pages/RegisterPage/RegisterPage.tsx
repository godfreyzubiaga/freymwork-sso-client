import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import {
  Button, Card, Divider, Form, Input, Select, Spin, Typography,
} from 'antd';
import styled from 'styled-components';

import { LoadingOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { appendTokenQuery, isDesktopOrLaptop } from '../../utils';
import { UserData } from '../../redux/Authorization/Authorization.types';
import { useTypedSelector } from '../../redux/root.reducer';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const { Title, Text } = Typography;
const { Option } = Select;

const StyledDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 1350px) {
    text-align: center;
  }
`;

const RegisterPage = () => {
  const { status: appStatus, application } = useTypedSelector((state) => state.applicationState);
  const { appId, status: authStatus, token } = useTypedSelector((state) => state.authState);
  const { status: configStatus } = useTypedSelector((state) => state.configState);
  // eslint-disable-next-line no-unused-vars
  const dispatch = useDispatch();
  const [role, setRole] = useState('User');

  const isLoading = () => appStatus.loading || authStatus.loading || configStatus.loading;

  const onFinish = (values: UserData) => {
    console.log({ ...values, role });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log(errorInfo);
  };

  const onRoleChange = (roleValue: string) => setRole(roleValue);

  const getQueryParams = () => {
    if (token) {
      return { token };
    }
    if (appId) {
      return { appId };
    }
    return {};
  };

  return (
    <StyledDiv>
      <Spin indicator={antIcon} spinning={isLoading()}>
        <Card
          bordered={false}
          style={{ height: 500, width: isDesktopOrLaptop() ? 400 : 'auto', textAlign: 'center' }}
        >
          <Title level={4}>{`${application?.appName} Register`}</Title>
          <Divider plain>
            <Text type="secondary">Freymwork SSO</Text>
          </Divider>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            style={{ textAlign: isDesktopOrLaptop() ? 'right' : 'center' }}
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: 'Please input your Name!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Username"
              name="username"
              rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              initialValue={role}
              wrapperCol={{ span: 6 }}
              labelCol={{ span: 6 }}
              name="role"
              label="Role"
            >
              <Select value={role} onChange={onRoleChange}>
                {application?.roles
                  && application?.roles.length > 0
                  && application?.roles.map((roles) => <Option value={roles}>{roles}</Option>)}
              </Select>
            </Form.Item>
            <Form.Item wrapperCol={isDesktopOrLaptop() ? { span: 24 } : {}}>
              <Button
                style={{ background: application?.colorTheme, border: 'none' }}
                type="primary"
                htmlType="submit"
                loading={authStatus.loading}
              >
                Register
              </Button>
              <div>
                <Text>
                  Already have an account?
                  <Link to={`/login${appendTokenQuery(getQueryParams())}`}>
                    <Button style={{ color: application?.colorTheme, padding: 0 }} type="link">
                      &nbsp;Login here
                    </Button>
                  </Link>
                </Text>
              </div>
            </Form.Item>
          </Form>
        </Card>
      </Spin>
    </StyledDiv>
  );
};

export default RegisterPage;
