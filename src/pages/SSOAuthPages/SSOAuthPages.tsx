import { Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';
import { useTypedSelector } from '../../redux/root.reducer';
import { extractDataFromToken } from '../../utils';
import AuthModalResult from './AuthModalResult';
import Unavailable from '../ErrorPages/Unavailable';

const LoginPage = React.lazy(() => import('../LoginPage/LoginPage'));
const RegisterPage = React.lazy(() => import('../RegisterPage/RegisterPage'));

interface IconDivProps {
  logoURL: string;
}

interface RightDivProps {
  bannerURL: string;
}

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

const LeftDiv = styled.div`
  width: 700px;
  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 1350px) {
    width: 100%;
  }
`;

const RightDiv = styled.div<RightDivProps>`
  height: 100%;
  width: calc(100% - 700px);
  display: flex;
  justify-content: center;
  align-items: center;
  background-image: url(${(props) => props.bannerURL});
  background-size: 600px auto;
  background-repeat: no-repeat;
  background-position: center center;
`;

const IconContainer = styled.div<IconDivProps>`
  height: 100px;
  width: 100px;
  background-image: url(${(props) => props.logoURL});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center center;
`;

interface Props {
  component?: string;
}

const SSOAuthPages = ({ component }: Props) => {
  const { application, status: appStatus } = useTypedSelector((state) => state.applicationState);
  const { authenticated, token, ssoToken } = useTypedSelector((state) => state.authState);
  const { status: configStatus } = useTypedSelector((state) => state.configState);
  const [flow, setFlow] = useState('');
  const navigate = useNavigate();

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 1350px)',
  });

  useEffect(() => {
    if (component) {
      setFlow(component);
    }
  }, [component]);

  const getComponent = () => {
    if (component === 'register') {
      if (application?.allowRegistration) {
        return <RegisterPage />;
      }
      navigate('/login');
    }
    if (component === 'login') {
      return <LoginPage />;
    }
    return <div>Not Found</div>;
  };

  const getRedirectURI = () => {
    const appData = extractDataFromToken(ssoToken);
    if (appData) {
      return `${appData.redirectURI}?token=${token}`;
    }
    return '';
  };

  // const isAnyLoading = () => appStatus.loading || authStatus.loading || configStatus.loading;

  return (
    <Container>
      <AuthModalResult
        appName={`${application?.appName}`}
        authenticated={authenticated}
        flow={flow}
        redirectURI={getRedirectURI()}
        subTitle="You're automatically logged in."
      />
      {!appStatus.error && !configStatus.error ? (
        <>
          <LeftDiv>
            <Space size={0} direction="vertical" align="center">
              <IconContainer logoURL={application?.logoURL || ''} />
              {getComponent()}
            </Space>
          </LeftDiv>
          {isDesktopOrLaptop && <RightDiv bannerURL={application?.bannerURL || ''} />}
        </>
      ) : (
        <>
          <LeftDiv>
            <Unavailable height="400px" width="500px" />
          </LeftDiv>
          <RightDiv bannerURL="https://res.cloudinary.com/freymwork/image/upload/v1638360451/Auth-Starksten/undraw_feeling_blue_-4-b7q_2_isbt9p.svg" />
        </>
      )}
    </Container>
  );
};

export default SSOAuthPages;
