import { Button, Modal, Result } from 'antd';
import * as React from 'react';

interface Props {
  redirectURI: string;
  authenticated: boolean;
  appName: string;
  flow: string;
  subTitle: string;
}

const AuthModalResult = (props: Props) => {
  const {
    redirectURI, appName, authenticated, flow, subTitle,
  } = props;
  const [time, setTime] = React.useState(5);

  const startCountdown = () => {
    if (time > 0) {
      setInterval(() => setTime(time - 1), 1000);
    } else {
      clearInterval();
      window.location.replace(redirectURI);
    }
  };

  const skipTimer = () => {
    clearInterval();
    window.location.replace(redirectURI);
  };

  React.useEffect(() => {
    if (authenticated) {
      startCountdown();
    }
  }, [time, authenticated]);

  return (
    <Modal
      closable={false}
      footer={[
        <Button type="primary" key="skip" onClick={skipTimer}>
          Skip
        </Button>,
      ]}
      visible={authenticated}
      style={{ textTransform: 'capitalize' }}
    >
      <Result
        status="success"
        title={`${flow} Successful!`}
        subTitle={`${subTitle} You will be redirected to ${appName} in ${time}s...`}
      />
    </Modal>
  );
};

export default AuthModalResult;
