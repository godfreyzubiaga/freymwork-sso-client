import { Typography } from 'antd';
import React from 'react';
import Particles from 'react-particles-js';
import styled from 'styled-components';
import useScreenSize from 'use-screen-size';

import { useTypedSelector } from '../../redux/root.reducer';

const { Title, Link } = Typography;

const StyledDiv = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: #333;
  color: #c4c4c4;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const InnerContainer = styled.div`
  z-index: 2;
  color: white;
  background-color: rgba(255, 255, 255, 0.9);
  height: 500px;
  width: 400px;
  border-radius: 10px;
  padding: 20px;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const Background = styled(Particles)`
  z-index: 1;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
`;

const Code = styled.code`
  color: black;
  margin: 5px;
`;

const LandingPage = () => {
  const { config } = useTypedSelector((state) => state.configState);
  const size = useScreenSize();

  return (
    <StyledDiv>
      <InnerContainer>
        <Title level={2}>Freymwork SSO</Title>
        <>
          <Code>
            I created this SSO Application for my personal projects. I enjoy making apps and a lot
            them has (atleast planned to have) a login/signup page.
          </Code>
          <Code>
            Then I wonder, why waste time implementing login/signup page for every personal
            application I make?
          </Code>
          <Code>
            Then this idea came to my mind; Create a Login/Signup app for all my projects 😊
          </Code>
        </>
        <Title level={5}>
          <Link
            style={{ whiteSpace: 'nowrap' }}
            href={config?.myLinks || 'https://solo.to/godfreyzubiaga'}
            target="_blank"
            rel="noopener noreferrer"
          >
            My Links →
          </Link>
        </Title>
      </InnerContainer>
      <Background
        params={{
          particles: {
            number: {
              value: size.width / 10,
            },
            size: {
              value: 5,
            },
          },
          interactivity: {
            events: {
              onhover: {
                enable: true,
                mode: 'grab',
              },
            },
          },
        }}
      />
    </StyledDiv>
  );
};

export default LandingPage;
