import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import store from './redux/store';
import { extractDataFromQuery } from './utils';

import './index.css';
import 'antd/dist/antd.css';

const token = extractDataFromQuery('token');

if (token) {
  localStorage.setItem('token', token);
}

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://snowpack.dev/concepts/hot-module-replacement
if (import.meta.hot) {
  import.meta.hot.accept();
}
