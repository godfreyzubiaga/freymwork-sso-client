import axios from 'axios';

// eslint-disable-next-line import/prefer-default-export
export const ssoCall = axios.create({
  baseURL: 'https://server.godfreyzubiaga.com/sso/',
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
  validateStatus: (status: number) => status < 500,
});
