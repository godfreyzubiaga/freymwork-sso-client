import { CallStatus, ErrorMessage } from '../types';

export const BEGIN_RETRIEVE_APPLICATION = '@@/BEGIN_RETRIEVE_APPLICATION';
export const SUCCESS_RETRIEVE_APPLICATION = '@@/SUCCESS_RETRIEVE_APPLICATION';
export const FAILED_RETRIEVE_APPLICATION = '@@/FAILED_RETRIEVE_APPLICATION';
export const BEGIN_RETRIEVE_APPLICATION_BY_ID = '@@/BEGIN_RETRIEVE_APPLICATION_BY_ID';
export const SUCCESS_RETRIEVE_APPLICATION_BY_ID = '@@/SUCCESS_RETRIEVE_APPLICATION_BY_ID';
export const FAILED_RETRIEVE_APPLICATION_BY_ID = '@@/FAILED_RETRIEVE_APPLICATION_BY_ID';

export interface BeginRetrieveApplicationAction {
  type: typeof BEGIN_RETRIEVE_APPLICATION;
}

export interface SuccessRetrieveApplicationAction {
  type: typeof SUCCESS_RETRIEVE_APPLICATION;
  payload: SearchApplicationResult;
}

export interface FaileRetrieveApplicationAction {
  type: typeof FAILED_RETRIEVE_APPLICATION;
  payload?: ErrorMessage;
}

export interface BeginRetrieveApplicationByIdAction {
  type: typeof BEGIN_RETRIEVE_APPLICATION_BY_ID;
}

export interface SuccessRetrieveApplicationByIdAction {
  type: typeof SUCCESS_RETRIEVE_APPLICATION_BY_ID;
  payload: ApplicationData;
}

export interface FaileRetrieveApplicationByIdAction {
  type: typeof FAILED_RETRIEVE_APPLICATION_BY_ID;
  payload?: ErrorMessage;
}

export type ApplicationActionTypes =
  | BeginRetrieveApplicationAction
  | SuccessRetrieveApplicationAction
  | FaileRetrieveApplicationAction
  | BeginRetrieveApplicationByIdAction
  | SuccessRetrieveApplicationByIdAction
  | FaileRetrieveApplicationByIdAction;

export interface ApplicationData {
  _id: string;
  appName: string;
  logoURL: string;
  bannerURL: string;
  favIcon: string;
  colorTheme: string;
  allowRegistration: boolean;
  redirectURI?: string;
  roles: string[];
}

export interface SearchApplicationResult {
  applications: ApplicationData[];
  count: number;
}

export interface IApplicationState {
  application?: ApplicationData;
  searchResult: SearchApplicationResult;
  status: CallStatus;
}
