import { failed, loading, success } from '../types';
import {
  ApplicationActionTypes,
  BEGIN_RETRIEVE_APPLICATION,
  BEGIN_RETRIEVE_APPLICATION_BY_ID,
  FAILED_RETRIEVE_APPLICATION,
  FAILED_RETRIEVE_APPLICATION_BY_ID,
  IApplicationState,
  SUCCESS_RETRIEVE_APPLICATION,
  SUCCESS_RETRIEVE_APPLICATION_BY_ID,
} from './Application.types';

const initialState: IApplicationState = {
  status: {
    loading: false,
  },
  searchResult: {
    applications: [],
    count: 0,
  },
};

const applicationReducer = (
  state = initialState,
  action: ApplicationActionTypes,
): IApplicationState => {
  switch (action.type) {
    case BEGIN_RETRIEVE_APPLICATION:
      return {
        ...state,
        status: { ...loading },
        searchResult: {
          applications: [],
          count: 0,
        },
      };
    case BEGIN_RETRIEVE_APPLICATION_BY_ID:
      return { ...state, status: { ...loading }, application: undefined };
    case SUCCESS_RETRIEVE_APPLICATION:
      return {
        ...state,
        status: { ...success },
        searchResult: { applications: action.payload.applications, count: action.payload.count },
      };
    case SUCCESS_RETRIEVE_APPLICATION_BY_ID:
      return { ...state, status: { ...success }, application: { ...action.payload } };
    case FAILED_RETRIEVE_APPLICATION:
    case FAILED_RETRIEVE_APPLICATION_BY_ID:
      return { ...state, status: { ...failed, error: action.payload } };
    default:
      return { ...state };
  }
};

export default applicationReducer;
