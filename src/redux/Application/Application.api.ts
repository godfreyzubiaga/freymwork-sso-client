import { ssoCall } from '../axiosCall';

export const getApplication = async (appName?: string) => {
  const { status, data } = await ssoCall.get(`applications/${appName}`);
  return { status, data };
};

export const getApplicationById = async (appId: string) => {
  const { status, data } = await ssoCall.get(`applications/id/${appId}`);
  return { status, data };
};
