/* eslint-disable import/prefer-default-export */
import { Dispatch } from 'react';
import {
  ApplicationActionTypes,
  BEGIN_RETRIEVE_APPLICATION_BY_ID,
  FAILED_RETRIEVE_APPLICATION_BY_ID,
  SUCCESS_RETRIEVE_APPLICATION_BY_ID,
} from './Application.types';
import { getApplicationById } from './Application.api';
import { showAPIError } from '../../utils';

export const retrieveApplicationById = (appId: string) => async (dispatch: Dispatch<ApplicationActionTypes>) => {
  try {
    dispatch({ type: BEGIN_RETRIEVE_APPLICATION_BY_ID });
    const { data, status } = await getApplicationById(appId);
    if (status === 200) {
      dispatch({
        type: SUCCESS_RETRIEVE_APPLICATION_BY_ID,
        payload: data.application,
      });
    } else {
      showAPIError(data.error.message);
      dispatch({
        type: FAILED_RETRIEVE_APPLICATION_BY_ID,
        payload: {
          code: data.error.code,
          message: data.error.message,
        },
      });
    }
  } catch (error) {
    dispatch({ type: FAILED_RETRIEVE_APPLICATION_BY_ID });
  }
};
