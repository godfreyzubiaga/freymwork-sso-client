import { TypedUseSelectorHook, useSelector } from 'react-redux';
import { combineReducers } from 'redux';
import applicationReducer from './Application/Application.reducer';
import authReducer from './Authorization/Authorization.reducer';
import configReducer from './Config/Config.reducer';
import { AppState } from './types';

export const useTypedSelector: TypedUseSelectorHook<AppState> = useSelector;

export const rootReducer = combineReducers({
  applicationState: applicationReducer,
  authState: authReducer,
  configState: configReducer,
});

export default rootReducer;
