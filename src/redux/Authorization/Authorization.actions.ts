import { notification } from 'antd';
import { Dispatch } from 'react';
import { showAPIError } from '../../utils';
import { ssoCall } from '../axiosCall';
import store from '../store';
import {
  AuthorizationActionType,
  BEGIN_LOGIN,
  SUCCESS_LOGIN,
  FAILED_LOGIN,
  LoginCredential,
  RegisterCredential,
  BEGIN_REGISTER,
  SUCCESS_REGISTER,
  FAILED_REGISTER,
  BEGIN_SAVE_TOKEN,
  SUCCESS_SAVE_TOKEN,
  FAILED_SAVE_TOKEN,
} from './Authorization.types';

export const saveTokenToStore = (token: string) => (dispatch: Dispatch<AuthorizationActionType>): void => {
  dispatch({ type: BEGIN_SAVE_TOKEN });
  try {
    if (token) {
      dispatch({ type: SUCCESS_SAVE_TOKEN, payload: { ssoToken: token } });
    }
  } catch (error) {
    dispatch({ type: FAILED_SAVE_TOKEN });
  }
};

export const loginAction = (userCredential: LoginCredential) => async (dispatch: Dispatch<AuthorizationActionType>): Promise<void> => {
  dispatch({ type: BEGIN_LOGIN });
  try {
    const axiosCall = await ssoCall.post('/auth/login', {
      ...userCredential,
      appId: store.getState().applicationState.application?._id,
    });
    if (axiosCall.status === 200 && axiosCall.data?.success) {
      dispatch({
        type: SUCCESS_LOGIN,
        payload: { user: axiosCall.data.user, token: axiosCall.data.token },
      });
    } else {
      showAPIError(axiosCall.data.error.message);
      dispatch({ type: FAILED_LOGIN, payload: axiosCall.data.error });
    }
  } catch (error) {
    notification.error({
      message: 'Login Error ❌',
      description: error as string,
      placement: 'bottomLeft',
    });
    dispatch({ type: FAILED_LOGIN });
  }
};

export const registerAction = (userData: RegisterCredential) => async (dispatch: Dispatch<AuthorizationActionType>): Promise<void> => {
  dispatch({ type: BEGIN_REGISTER });
  try {
    const axiosCall = await ssoCall.post('/auth/register', { ...userData });
    if (axiosCall.status === 200 && axiosCall.data?.success) {
      dispatch({
        type: SUCCESS_REGISTER,
        payload: { user: axiosCall.data.user, token: axiosCall.data.token },
      });
    } else {
      showAPIError(axiosCall.data.error.message);
    }
  } catch (error) {
    dispatch({ type: FAILED_REGISTER });
  }
};
