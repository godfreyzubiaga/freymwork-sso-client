import { CallStatus, ErrorMessage } from '../types';

export const BEGIN_LOGIN = '@@/BEGIN_LOGIN';
export const SUCCESS_LOGIN = '@@/SUCCESS_LOGIN';
export const FAILED_LOGIN = '@@/FAILED_LOGIN';
export const BEGIN_REGISTER = '@@/BEGIN_REGISTER';
export const SUCCESS_REGISTER = '@@/SUCCESS_REGISTER';
export const FAILED_REGISTER = '@@/FAILED_REGISTER';
export const BEGIN_SAVE_TOKEN = '@@/BEGIN_SAVE_TOKEN';
export const SUCCESS_SAVE_TOKEN = '@@/SUCCESS_SAVE_TOKEN';
export const FAILED_SAVE_TOKEN = '@@/FAILED_SAVE_TOKEN';

export interface BeginLoginAction {
  type: typeof BEGIN_LOGIN;
}

export interface SuccessLoginAction {
  type: typeof SUCCESS_LOGIN;
  payload: AuthData;
}

export interface FailedLoginAction {
  type: typeof FAILED_LOGIN;
  payload?: ErrorMessage;
}

export interface BeginRegisterAction {
  type: typeof BEGIN_REGISTER;
}

export interface SuccessRegisterAction {
  type: typeof SUCCESS_REGISTER;
  payload: AuthData;
}

export interface FailedRegisterAction {
  type: typeof FAILED_REGISTER;
}

export interface BeginSaveTokenAction {
  type: typeof BEGIN_SAVE_TOKEN;
}

export interface SuccessSaveTokenAction {
  type: typeof SUCCESS_SAVE_TOKEN;
  payload: object;
}

export interface FailedSaveTokenAction {
  type: typeof FAILED_SAVE_TOKEN;
}

export type AuthorizationActionType =
  | BeginLoginAction
  | BeginRegisterAction
  | BeginSaveTokenAction
  | SuccessLoginAction
  | SuccessRegisterAction
  | SuccessSaveTokenAction
  | FailedLoginAction
  | FailedRegisterAction
  | FailedSaveTokenAction;

export interface AuthData {
  user: UserData;
  token: string;
}
export interface UserData {
  _id: string;
  name: string;
  username: string;
  role: string;
  application: string;
}

export interface LoginCredential {
  username: string;
  password: string;
}

export interface RegisterCredential {
  username: string;
  password: string;
  name: string;
  role: string;
}

export interface IAuthState {
  user?: UserData;
  authenticated: boolean;
  status: CallStatus;
  token: string;
  ssoToken: string;
  appId: string;
}
