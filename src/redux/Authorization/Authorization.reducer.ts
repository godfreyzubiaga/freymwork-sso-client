import { failed, loading, success } from '../types';
import {
  AuthorizationActionType,
  BEGIN_LOGIN,
  BEGIN_SAVE_TOKEN,
  FAILED_LOGIN,
  FAILED_SAVE_TOKEN,
  IAuthState,
  SUCCESS_LOGIN,
  SUCCESS_SAVE_TOKEN,
} from './Authorization.types';

const initialState: IAuthState = {
  status: {
    loading: false,
  },
  user: undefined,
  authenticated: false,
  token: '',
  ssoToken: '',
  appId: '',
};

const authReducer = (state = initialState, action: AuthorizationActionType): IAuthState => {
  switch (action.type) {
    case BEGIN_LOGIN:
    case BEGIN_SAVE_TOKEN:
      return { ...state, status: loading };
    case SUCCESS_LOGIN:
      return {
        ...state,
        status: success,
        user: action.payload.user,
        token: action.payload.token,
        authenticated: true,
      };
    case SUCCESS_SAVE_TOKEN:
      return { ...state, status: success, ...action.payload };
    case FAILED_LOGIN:
      return { ...state, status: { ...failed, error: action.payload } };
    case FAILED_SAVE_TOKEN:
      return { ...state, status: failed };
    default:
      return { ...state };
  }
};

export default authReducer;
