import { ssoCall } from '../axiosCall';
import { LoginCredential, RegisterCredential } from './Authorization.types';

export const validateToken = async (token: string) => {
  const { status, data } = await ssoCall.get(`auth/validate/${token}`);
  return { status, data };
};

export const login = async (userCredential: LoginCredential) => {
  const { status, data } = await ssoCall.post(`auth/login${userCredential}`);
  return { status, data };
};

export const register = async (userData: RegisterCredential) => {
  const { status, data } = await ssoCall.post(`auth/register${userData}`);
  return { status, data };
};
