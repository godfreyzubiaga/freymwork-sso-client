/* eslint-disable import/prefer-default-export */
import { Dispatch } from 'redux';
import { ssoCall } from '../axiosCall';
import {
  BEGIN_RETRIEVE_CONFIG,
  ConfigActionTypes,
  FAILED_RETRIEVE_CONFIG,
  SUCCESS_RETRIEVE_CONFIG,
} from './Config.types';

export const retrieveConfig = () => async (dispatch: Dispatch<ConfigActionTypes>) => {
  try {
    dispatch({ type: BEGIN_RETRIEVE_CONFIG });
    const axiosCall = await ssoCall.get('/config');
    // const axiosCall2 = await axios.get('/appConfig/config.json');
    // console.log('asda', process);
    // console.log('aaaaaaa', axiosCall2.data);
    const { data, status } = axiosCall;
    if (status === 200) {
      dispatch({ type: SUCCESS_RETRIEVE_CONFIG, payload: { ...data.config } });
    } else {
      dispatch({
        type: FAILED_RETRIEVE_CONFIG,
        payload: {
          code: data.error.code,
          message: data.error.message,
        },
      });
    }
  } catch (error) {
    console.log({ error });
    dispatch({ type: FAILED_RETRIEVE_CONFIG });
  }
};
