import { CallStatus, ErrorMessage } from '../types';

export const BEGIN_RETRIEVE_CONFIG = '@@/BEGIN_RETRIEVE_CONFIG';
export const SUCCESS_RETRIEVE_CONFIG = '@@/SUCCESS_RETRIEVE_CONFIG';
export const FAILED_RETRIEVE_CONFIG = '@@/FAILED_RETRIEVE_CONFIG';

export interface BeginRetrieveConfigAction {
  type: typeof BEGIN_RETRIEVE_CONFIG;
}

export interface SuccessRetrieveConfigAction {
  type: typeof SUCCESS_RETRIEVE_CONFIG;
  payload: ConfigData;
}

export interface FaileRetrieveConfigAction {
  type: typeof FAILED_RETRIEVE_CONFIG;
  payload?: ErrorMessage;
}

export type ConfigActionTypes =
  | BeginRetrieveConfigAction
  | SuccessRetrieveConfigAction
  | FaileRetrieveConfigAction;

export interface IConfigState {
  config?: ConfigData;
  status: CallStatus;
}

export interface ConfigData {
  defaultAppId: string;
  myLinks: string;
}
