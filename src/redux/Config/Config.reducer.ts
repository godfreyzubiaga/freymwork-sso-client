import {
  BEGIN_RETRIEVE_CONFIG,
  ConfigActionTypes,
  FAILED_RETRIEVE_CONFIG,
  IConfigState,
  SUCCESS_RETRIEVE_CONFIG,
} from './Config.types';

const initialState: IConfigState = {
  status: {
    loading: false,
  },
};

const configReducer = (state = initialState, action: ConfigActionTypes): IConfigState => {
  switch (action.type) {
    case BEGIN_RETRIEVE_CONFIG:
      return { ...state, status: { loading: true } };
    case SUCCESS_RETRIEVE_CONFIG:
      return { ...state, status: { loading: false }, config: action.payload };
    case FAILED_RETRIEVE_CONFIG:
      return { ...state, status: { loading: false, error: action.payload } };
    default:
      return { ...state };
  }
};

export default configReducer;
