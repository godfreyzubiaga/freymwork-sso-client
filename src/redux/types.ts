import { IApplicationState } from './Application/Application.types';
import { IAuthState } from './Authorization/Authorization.types';
import { IConfigState } from './Config/Config.types';

export interface ReduxAction {
  type: string;
  payload: unknown;
}

export interface ErrorMessage {
  code: number;
  message: string;
}

export interface CallStatus {
  loading: boolean;
  success?: boolean;
  error?: ErrorMessage;
}

export const loading: CallStatus = { loading: true, error: undefined };
export const success: CallStatus = { loading: false, success: true };
export const failed: CallStatus = { loading: false, success: false };

export interface AppState {
  applicationState: IApplicationState;
  authState: IAuthState;
  configState: IConfigState;
}
